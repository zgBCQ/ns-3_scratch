
#include "ns3/core-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("ScratchSimulator");

int
main(int argc, char* argv[])
{
    NS_LOG_UNCOND("Scratch Simulator hello world!!!!");
    NS_LOG_UNCOND("S123");

    Simulator::Run();
    Simulator::Destroy();

    return 0;
}